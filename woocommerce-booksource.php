<?php
/*
Plugin Name:  Booksource for WooCommerce
Plugin URI: http://www.enru.co.uk/wordpress/woocommerce-booksource/
Description: Exports orders as XML for Booksource 
Author: Neill Russell
Author URI: http://www.enru.co.uk/
Version: 0.1
*/

require plugin_dir_path(__FILE__).'classes/class-booksource-admin.php';
new Booksource_Admin('booksource');

// downloads XML order
add_action('request', function($request) {
    if(isset($_GET['download']) && $_GET['download'] == 'booksource-xml') {
        if(isset($_GET['order']) && $_GET['order']) {
            // only allow admins to print orders
            if(current_user_can('manage_options')) {
                require plugin_dir_path(__FILE__).'classes/class-booksource.php';
                $booksource= new Booksource();
                $booksource->output($_GET['order']);
            }
        }
    }
    if(isset($_GET['export']) && $_GET['export'] == 'booksource-xml') {
        if(isset($_GET['order']) && $_GET['order']) {
            // only allow admins to print orders
            if(current_user_can('manage_options')) {
                require plugin_dir_path(__FILE__).'classes/class-booksource.php';
                $booksource= new Booksource();
                $booksource->export($_GET['order']);

                // redirect to order page
                wp_redirect(admin_url('post.php?action=edit&post='.$_GET['order']));
                exit;
            }
        }
    }
    return $request;
});

// adds download/export links in order admin
add_action('woocommerce_admin_order_data_after_order_details', function() {

    $url = sprintf('/?download=booksource-xml&order=%d',$_GET['post']);
    echo '<p class="form-field form-field-wide"><a href="'.home_url($url).'">Download Booksource XML</a></p>';

    $url = sprintf('/?export=booksource-xml&order=%d',$_GET['post']);
    echo '<p class="form-field form-field-wide"><a href="'.home_url($url).'">FTP Booksource XML</a></p>';
});

// exports order on payment complete
add_action('woocommerce_order_status_processing', function ($order_id) {

    require plugin_dir_path(__FILE__).'classes/class-booksource.php';
    $booksource= new Booksource();
    $booksource->export($order_id);

    return $order_id;
}, 10, 1);


// set up importer 
require plugin_dir_path(__FILE__).'classes/class-booksource-importer.php';
global $booksource_importer;
$booksource_importer = new Booksource_Importer();

// clear scheduled hook when deactivated
register_deactivation_hook( __FILE__, function() {
    wp_clear_scheduled_hook( 'booksource_import' );
});


