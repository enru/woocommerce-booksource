<?php 

class Booksource {

    private $ftp = array();
    private $countries;

    public function __construct() {

        $this->ftp = array(
            'user' => get_option('booksource_ftp_user'),
            'pass' => get_option('booksource_ftp_password'),
            'host' => get_option('booksource_ftp_host'),
            'port' => get_option('booksource_ftp_port', 21),
        );

        if(!class_exists('Booksource_Countries')) {
            require plugin_dir_path(__FILE__).'class-booksource-countries.php';
            $this->countries = new Booksource_Countries();
        }
    }

    private function build_xml($order) {

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="ISO-8859-1"?><Order></Order>');

        $header = $xml->addChild('OrderHeader');

        $billing_country = $this->countries->get_country($order['billing_country']);
        $shipping_country = $this->countries->get_country($order['shipping_country']);

        $nodes = array(
            'XEACREF' => substr($order['id'], 0,30),
            'XEACN' => substr(get_option('booksource_account_id'), 0, 10), // account number
            'XOIOCNM' => substr(sprintf("%s %s", $order['billing_first_name'], $order['billing_last_name']), 0, 30), 
            'XEACRFD' => substr($order['order_date'], 0, 10), 
            'XOIOAD1' => substr($order['billing_company'] ? $order['billing_company'] : $order['billing_address_1'], 0, 30), 
            'XOIOAD2' => substr($order['billing_company'] ? $order['billing_address_1'] : $order['billing_address_2'], 0, 30), 
            'XOIOAD3' => substr($order['billing_company'] ? $order['billing_address_2'] : '', 0, 30), 
            'XOIOAD4' => substr($order['billing_city'], 0, 20),
            'XOIOAD5' => substr($order['billing_postcode'], 0, 9),
            'XOIOAD6' => substr($billing_country, 0, 30),
            'XOIOPHN' => substr($order['billing_phone'], 0, 20),
            'XOEETEXT' => substr($order['billing_email'], 0, 80),
            'XEADCNM' => substr(sprintf("%s %s", $order['shipping_first_name'], $order['shipping_last_name']), 0, 30), 
            'XEADAD1' => substr($order['shipping_company'] ? $order['shipping_company'] : $order['shipping_address_1'], 0, 30), 
            'XEADAD2' => substr($order['shipping_company'] ? $order['shipping_address_1'] : $order['shipping_address_2'], 0, 30), 
            'XEADAD3' => substr($order['shipping_company'] ? $order['shipping_address_2'] : '', 0, 30), 
            'XEADAD4' => substr($order['shipping_city'], 0, 20),
            'XEADAD5' => substr($order['shipping_postcode'], 0, 9),
            'XEADAD6' => substr($shipping_country, 0, 30),
            'XEASI1' => substr('', 0, 40), // order notes 1
            'XEASI2' => substr('', 0, 40), // order notes 2
            'XT6PAYT' => substr(preg_match('/paypal/i', $order['payment_method']) ? 'PAY' : 'ST', 0, 3),
        );

        if(get_option('booksource_paid_to_publisher')) $nodes['XT6PAYT'] = 'PTP'; 

        foreach($nodes as $child => $data) {
            $header->addChild($child, $data);
        }
    
        // get the configured ISBN custom field names
        //$isbn10_cf = get_option('booksource_isbn10_custom_field');
        $isbn13_cf = get_option('booksource_isbn13_custom_field');

        foreach($order['items'] as $order_item) {

            $item_id = isset($order_item['id']) ? $order_item['id'] : $order_item['product_id'];

            // populate the ISBN EAN13 field
            $isbn = ''; //get_post_meta($item_id, $isbn10_cf, true);
            if($isbn13 = get_post_meta($item_id, $isbn13_cf, true)) {
                $isbn = $isbn13;
            }
            // ensure ISBN is numercial only
            $isbn = preg_replace("/[^0-9]/","",$isbn);

            $item = $xml->addChild('OrderItem');

            $nodes = array(
                'XEBI' => substr($isbn, 0, 14),
                'XEBORDQ' => substr($order_item['qty'], 0, 13),
                'XEBUPRA' => substr($order_item['line_subtotal'], 0, 9),
            );

            foreach($nodes as $child => $data) {
                $item->addChild($child, $data);
            }
        }

        return $xml->asXml();

    }

    private function order_as_array($order_id) {

        if(function_exists('wc_get_order')) {
            $order = wc_get_order($order_id);
        }
        else {
            $order = new WC_Order( $order_id );
        }

        $data = (array) $order;
        $data['items'] = $order->get_items();

        // force setting addresses for WC < 2.3
        $address = array(
            'first_name',
            'last_name',
            'company',
            'address_1',
            'address_2',
            'city',
            'state',
            'postcode',
            'country',
            'phone',
            'email',
         );

        // populate the addresses
        foreach(array('billing', 'shipping') as $type) {
            foreach($address as $a) {
                $key = $type.'_'.$a;
                // all address details to be in uppercase
                $data[$key] = strtoupper($order->$key);
            }
        }

        $data['payment_method'] = $order->payment_method;

        //error_log(var_export($order, true));
        //error_log(var_export($data, true));
        return $data;
    }

    // given an array of order data...
    // returns file name in this format ORD[XEACN][XEACREF].XML
    private function gen_filename($order_id) {
        $XEACREF = $order_id;
        $XEACN = get_option('booksource_account_id');
        return sprintf('ORD%s%s.XML', $XEACN, $XEACREF);
    }

    // outputs an order as XML
    function output($order_id) {

        $data = $this->order_as_array($order_id);
        $filename = $this->gen_filename($order_id);

        header("Content-type: text/xml",true,200);
        header("Content-Disposition: attachment; filename=".$filename);
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $this->build_xml($data);
        exit;
    }

    /// sends an order via FTP to booksource
    function export($order_id) {
                 
        if(function_exists('wc_get_order')) {
            $order = wc_get_order($order_id);
        }
        else {
            $order = new WC_Order( $order_id );
        }

        $status = get_post_meta( $order_id, 'booksource_status', true);

        // only send if order is NOT paid && if we DON'T have a status flag already
        if($order->needs_payment()) {
            wp_die('Order needs to be paid before sending to Booksource.');
        }
        elseif(!$order->has_status('processing')) {
            wp_die('Order needs to be in status "processing" to send to Booksource.');
        }
        elseif(strlen($status) > 0) {
            wp_die('Order has been sent to Booksource already');
        }
        else {
 
            $data = $this->order_as_array($order_id);

            $xml= $this->build_xml($data);

            // copy XML to a tmp file
            $filename = $this->gen_filename($order_id);
            $tmp = tempnam("/tmp", $filename);
            $fh= fopen($tmp, "w");
            fwrite($fh, $xml);
             
            $this->ftp($tmp, $filename);

            fclose($fh);

            $this->mark_as_exported($order_id);

        }
    }

    private function ftp($local, $remote) {

        if($conn_id = ftp_connect($this->ftp['host'], $this->ftp['port'])) {

            if($login = ftp_login($conn_id, $this->ftp['user'], $this->ftp['pass'])) {

                // upload a file
                if (ftp_put($conn_id, $remote, $local, FTP_ASCII)) 
                    error_log(sprintf("successfully uploaded %s to %s@%s:%s:%s\n", $local, $this->ftp['user'], $this->ftp['host'], $this->ftp['port'], $remote));
                else  error_log("There was a problem while uploading $local");
            }

            ftp_close($conn_id);
        }
    }

    function mark_as_exported($order_id, $msg = null) {

        update_post_meta($order_id, 'booksource_status', 'exported: '.date('Y-m-d H:m:i', time()));

        $msg = sprintf("Order exported to Booksource %s", ($msg ? " :$msg" : ""));

        wp_insert_comment(array(
            'comment_post_ID' => $order_id,
            'comment_author' => 'Booksource',
            'comment_content' => $msg,
            'comment_type' => 'order_note',
            'comment_parent' => 0,
            'comment_agent' => 'Booksource',
            'comment_date' => current_time('mysql'),
            'comment_approved' => 1,
        ));

    }

}





