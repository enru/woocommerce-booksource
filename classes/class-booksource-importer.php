<?php

class Booksource_Importer {

    private $status = array(
        'B'=>'TITLE ABANDONED',
        'D'=>'PRINT ON DEMAND',
        'F'=>'TEMPORARILY FROZEN',
        'L'=>'NO LONGER STOCKED(1)',
        'M'=>'NO LONGER STOCKED(2)',
        'N'=>'NOT OUR PRODUCT',
        'O'=>'OUT OF PRINT',
        'P'=>'PUBLISHED',
        'Q'=>'ON-LINE ONLY',
        'R'=>'OUT OF STOCK',
        'S'=>'AWAITING RELEASE',
        'T'=>'REPRINTING',
        'V'=>'REFER TO PUBLISHER',
        'W'=>'OUT OF STOCK',
        'Y'=>'NOT YET PUBLISHED',
        'Z'=>'AWAITING RELEASE',
    );

    function __construct() {
        // set up the auto import
        add_action("booksource_import", array($this, 'import'));
        // schedule the auto-import
        add_action("init", array($this, 'run_auto_import'));
    }

    /**
     * schedule events 
     */
    function run_auto_import() {
        //echo "<pre>";
        //echo var_export(_get_cron_array(), true);
        if(!wp_next_scheduled('booksource_import')) {
            wp_schedule_event(time(), 'daily', 'booksource_import');
        }
    }

    /** 
     * update products 

     */
    function import($verbose=false) {

        $imported = 0;

        if($verbose) echo '<pre>';

        if($product_file = get_option('booksource_product_file_path')) {

            if(file_exists($product_file)) {

                $row = 0;
                if (($handle = fopen($product_file, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle)) !== FALSE) {
                        $row += 1;
                        if ($row < 2) continue;
                        if($verbose)  printf("Received ISBN13 %s", $data[0]);
                        error_log(sprintf("Received ISBN13 %s", $data[0]));
                        // find product by ISBN and update stock, etc
                        if($updated = $this->update_product($data)) {
                            if($verbose) echo " - Updated";
                            error_log(" - Updated");
                            $imported++;
                        }
                        if($verbose)  echo "\n";

                    }
                    fclose($handle);
                }
            }
        }
        elseif($verbose) {
            echo 'No product file found :(';
        }

        // end property listing
        if($verbose) echo '</pre>';

        return $imported;
    }

    /**
     * find a product by ISBN 13 and update it
     * products are like so:
     *  array (
          0 => 'ISBN',
          1 => 'TITLE',
          2 => 'STOCK STATUS',
          3 => 'RRP',
          4 => 'AVAILABLE',
          5 => 'PUB DATE',
     * ) 
     */
    private function update_product($data) {

        $updated = false;

        $pubdate_cf = get_option('booksource_publication_date_custom_field');

        if($isbn13_cf = get_option('booksource_isbn13_custom_field')) {

            // find the post
            $posts = new WP_Query(array(
                'post_type' => 'product',
                //'meta_key' => $isbn13_cf,
                //'meta_value' => $data[0],
                'meta_query' => array(array (
                    'key' => $isbn13_cf,
                    'value' => array($data[0], $this->format_isbn13($data[0])),
                    'compare' => 'IN'
                )),
            ));

            // update the post
            if($posts->post_count > 0 && $posts->post) {

                // update the stock qty
                update_post_meta( $posts->post->ID, '_stock', $data[4]);

                // update the stock status
                $status = in_array($data[2], array('Q','P')) ? 'instock' : 'outofstock';
                update_post_meta( $posts->post->ID, '_stock_status', $status );

                // update the publication date
                $publication_date  = preg_replace('/[^0-9]/', '', $data[5]);
                update_post_meta($posts->post->ID, $pubdate_cf, $publication_date);

                $updated = true;

            }
        }

        return $updated;

    }

    /**
     * converts 9781909141292 to 978-1-909141-29-2
     */
    private function format_isbn13($isbn) {
        return sprintf('%d-%d-%d-%d-%d', 
            substr($isbn, 0,3),
            substr($isbn, 3,1),
            substr($isbn, 4,6),
            substr($isbn, 10,2),
            substr($isbn, 12,1));
    }

} // end of Booksource Importer

     
