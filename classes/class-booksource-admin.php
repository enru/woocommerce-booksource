<?php
/*
Author: Neill Russell (@enru)  
Author URI: http://enru.co.uk/
*/

class Booksource_Admin {

    // variables for the field and option names 
    private  $options = array(
        'Account Id',
        'FTP User',
        'FTP Password',
        'FTP Host',
        'FTP Port',
        //'ISBN10 Custom Field',
        'ISBN13 Custom Field',
        'Publication Date Custom Field',
        'Paid to Publisher',
        'Product file path',
    );

    private $slug;

    function __construct($slug) {
        $this->slug = $slug;
        add_action('admin_menu', array($this, 'init'), 9); 
        add_action('admin_head', array($this, 'head'));
    }

    function head() {
        // drop in our admin styles
        echo "\n<style type=\"text/css\">\n";
        echo "form[name=\"".$this->slug."-form\"] { width: 100%; }\n";
        echo "form[name=\"".$this->slug."-form\"] label { width: 25%; float: left; clear: left; margin-bottom: 20px;}\n";
        echo "form[name=\"".$this->slug."-form\"] input { float: left; }\n";
        echo "form[name=\"".$this->slug."-form\"] select { float: left; }\n";
        echo "form[name=\"".$this->slug."-form\"] input[type=\"submit\"] { clear: left; }\n";
        echo "</style>\n";

        // do some JS stuff
        echo "\n<script type=\"text/javascript\"> jQuery(document).ready(function($) { 
        });</script>\n";

    }

    function init() {

        add_menu_page(
            'Booksource', //page title
            'Booksource', //menu title
            'edit_posts', //capability
            $this->slug, //menu slug
            array($this, 'dash'), //function
            null, //icon url
            0 //pos
        );

        // options page
        add_submenu_page(
            $this->slug, //parent slug
            'Booksource Settings', //page title
            'Settings', //menu title
            'manage_options', //capability
            'booksource-options', //menu slug
            array($this, 'menu') //function
        );

        // import page
        add_submenu_page(
            $this->slug, //parent slug
            'Booksource Import', //page title
            'Import', //menu title
            'manage_options', //capability
            'booksource-import', //menu slug
            array($this, 'import') //function
        );
         
    }

    // our dashboard
    function dash() {
        if (!current_user_can('manage_options')) {
            wp_die( __('You do not have sufficient permissions to access this page.') );
        }
        echo '<h2>Booksource</h2>';
        echo '<p>Welcome to the Booksource WordPress plugin</p>';
        echo '<p>You can update your plugin settings here:</p>';
        echo '<p>&nbsp;<a href="'.admin_url('admin.php?page=booksource-options').'">'.admin_url('admin.php?page=booksource-options').'</a>';
    }

    // displays the page content for this plugin's settings
    function menu() {

        // must check that the user has the required capability
        // manage_options = admin only
        if (!current_user_can('manage_options')) {
            wp_die( __('You do not have sufficient permissions to access this page.') );
        }

        // now display the settings editing screen
        echo '<div class="wrap">';

        // header
        echo "<h2>" . __( 'Settings', $this->slug.'-admin' ) . "</h2>";

        // settings form
        echo '<form name="'.esc_attr($this->slug).'-form" method="post" action="">';

        echo $this->option_form_fields();
        
        echo '<p class="submit">';
        echo '<input type="submit" name="Submit" class="button-primary" value="' . esc_attr('Save Changes') . '" />';
        echo '</p>';
        echo '</form>';
        echo '</div>';
    }    

    // output option form fields
    private function option_form_fields() {

        $html = '';
        $_POST   = stripslashes_deep($_POST);
        foreach($this->options as $display_field_name) {

            $opt = strtolower(str_replace(' ', '_', preg_replace('/[^\x00-\x7F]+/', '', $display_field_name)));

            $opt_name = $data_field_name = $this->slug.'_' . $opt;
            $hidden_field_name = $opt . '_submit_hidden';

            // Read in existing option value from database
            $opt_val = get_option( $opt_name );
    
            // See if the user has posted us some information
            // If they did, this hidden field will be set to 'Y'
            if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
                // Read their posted value
                $opt_val = $_POST[ $data_field_name ];
    
                // Save the posted value in the database
                update_option( $opt_name, $opt_val );
    
            }

            $html .= '<input type="hidden" name="' . esc_attr($hidden_field_name) . '" value="Y">';
            $html .= '<label for="'.esc_attr($data_field_name).'">' .  __($display_field_name. ':', $this->slug.'-admin' );
            $html .= '</label>';

            // ISBN or Publication Date
            if(preg_match('/(isbn|publication date)/i', $display_field_name)) {
                $html .= '<select id="'.esc_attr($data_field_name).'" name="'.esc_attr($data_field_name).'">';
                $html .= '<option value="">'.__("Select the custom field", $this->slug.'-admin').'</option>';
                $custom_field_names = $this->get_custom_fields();
                foreach($custom_field_names as $name){
                    $html .= '<option value="'.esc_attr($name).'" '.selected($name, $opt_val,false).'>'.esc_html($name).'</option>';
                }        
                $html .= '</select>';
            }
            // PAID TO PUBLISHER
            elseif(preg_match('/paid/i', $display_field_name)) {
                $html .= "<label>(Paid using 3rd party Payment Gateway not Booksource)</label>";
                $html .= '<input type="checkbox"';
                $html .= ' id="'.esc_attr($data_field_name).'" name="' . esc_attr($data_field_name) . '"';
                $html .= ' value="1" ' . checked($opt_val, 1, false) . '/>';
            }
            // PASSWORD 
            elseif(preg_match('/password/i', $display_field_name)) {
                $html .= '<input type="password"';
                $html .= ' id="'.esc_attr($data_field_name).'" name="' . esc_attr($data_field_name) . '"';
                $html .= ' value="' . esc_attr($opt_val) . '" size="50"/>';
            }
            // DEFAULT - text field
            else {
                $html .= '<input type="text"';
                $html .= ' id="'.esc_attr($data_field_name).'" name="' . esc_attr($data_field_name) . '"';
                $html .= ' value="' . esc_attr($opt_val) . '" size="50"/>';
            }

        } 
        return $html;
    }

    // gets all custom fields link to product post type
    private function get_custom_fields(){
        global $wpdb;
        $keys = $wpdb->get_col( "
        SELECT DISTINCT m.meta_key
        FROM $wpdb->postmeta m 
        JOIN $wpdb->posts p ON (p.id = m.post_id and p.post_type = 'product')
        WHERE m.meta_key NOT LIKE '\_%'
        GROUP BY m.meta_key
        ORDER BY m.meta_id DESC");

        if ( $keys )
            natcasesort($keys);

        return $keys;
    }

    function import() {

        //must check that the user has the required capability 
        if (!current_user_can('edit_posts')) {
            wp_die( __('You do not have sufficient permissions to access this page.') );
        }

        if(isset($_POST['import']) && $_POST['import'] == 1) {

            $make_csv = isset($_POST['make_csv']);
            global $booksource_importer;

            if(!class_exists('Booksource_Importer') || $booksource_importer == null) {
                require_once plugin_dir_path(__FILE__).'class-booksource-importer.php';
                $booksource_importer = new Booksource_Importer();
            }

            echo '<div class="wrap">';
            echo "<h2>" . __( 'Importing', $this->slug.'-admin' ) . "</h2>";
            echo '<p id="msg">importing...</br>please wait...</p>';
            $next_url = 'edit.php?post_type=product';
            echo '<p id="nextlink" style="display:none"><a href="'.admin_url($next_url).'">view products</a></p>';
            $imported = $booksource_importer->import($verbose=true);
            echo '</div>';

            echo '<script>jQuery("#msg").html("'.esc_js($imported).' products imported."); jQuery("#nextlink").show();</script>';
        }
        else {
            echo '<div class="wrap">';
            echo '<h2>' . __( 'Booksource Importer', $this->slug.'-admin' ) . '</h2>';

            // action to process file & import properties
            echo '<form name="form2" method="post" action="">';
            echo '<input type="hidden" name="import" value="1"/>';
            echo '<p class="submit">';
            echo '<input type="submit" name="Submit" class="button-primary" value="' . esc_attr('Import Product Stocks') . '" />';
            echo '</p>';
            echo '</form>';

            echo '</div>';
        }

    }
}
